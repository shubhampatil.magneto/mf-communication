import React from "react";

function Header() {
  return (
    <header className="header">
      <h1>KGK Header</h1>
      <nav className="nav">
        <ul className="nav-list">
          <li>
            <a href="/">Home</a>
          </li>
          <li>
            <a href="/catalogue">Catalogue</a>
          </li>
          <li>
            <a href="/moodboard">Moodboard</a>
          </li>
          <li>
            <a href="/cart">Cart</a>
            <span> 0</span>
          </li>
        </ul>
      </nav>
      <style jsx>{`
        .header {
          background-color: #020cc9;
          color: white;
          padding: 1rem;
          display: flex;
          justify-content: space-between;
          align-items: center;
        }

        .header h1 {
          margin: 0;
          font-size: 2rem;
        }

        .nav {
            display: flex;
            flex-direction: row;
        }

        .nav-list {
            list-style: none;
            display: flex;
            flex-direction: row;
        }
        .nav-list li {
            padding: 20px;
            color: #fff;
        }
        .nav-list li a {
            color: #fff;
        }
      `}</style>
    </header>
  );
}

export default Header;
